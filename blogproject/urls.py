"""blogproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from blog import urls as blog_urls
from django.views.generic import RedirectView,TemplateView
from bloguser import urls as user_urls
from blog.feeds import AtomPostFeed,Rss2PostFeed
from django.contrib.sitemaps.views import (
    index as site_index_view,
    sitemap as sitemap_view)
from .sitemaps import sitemaps as sitemaps_dict


admin.site.site_header='Blog Project Admin'
admin.site.site_title='Blog Project Site Admin'

sitenews=[
    url(r'^atom/$',
        AtomPostFeed(),
        name='blog_atom_feed'),
    url(r'^rss/$',
        Rss2PostFeed(),
        name='blog_rss_feed'),
]

urlpatterns = [
    url(r'^$',RedirectView.as_view(
        pattern_name='blog_post_list',
        permanent=False)),
    url(r'^admin/', admin.site.urls),
    url(r'^blog/', include(blog_urls)),
    url(r'^user/', include(
        user_urls,
        app_name='bloguser',
        namespace='dj-auth')),
    url(r'^sitenews/',include(sitenews)),
    url(r'^sitemap\.xml$',
        site_index_view,
        {'sitemaps':sitemaps_dict},
        name='sitemap'),
    url(r'^sitemap-(?P<section>.+)\.xml$',
        sitemap_view,
        {'sitemaps':sitemaps_dict},
        name='sitemap-sections'),
]
