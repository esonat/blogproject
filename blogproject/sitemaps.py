from blog.sitemaps import PostSitemap,CommentSitemap
from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse

class RootSitemap(Sitemap):
    priority=0.6

    def items(self):
        return [
            'blog_post_list',
            'dj-auth:login',
        ]
    def location(self, url_name):
        return reverse(url_name)

sitemaps={
    'posts': PostSitemap,
    'comments':CommentSitemap,
    'roots': RootSitemap,
}