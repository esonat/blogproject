from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from datetime import date
import uuid


class BasePostManager(models.Manager):
    def get_queryset(self):
        return(
            PostQueryset(
                self.model,
                using=self._db,
                hints=self._hints)
            .select_related('author__profile'))


    def get_by_natural_key(self,pub_date,slug):
        return self.get(
            pub_date=pub_date,
            slug=slug)

class PostQueryset(models.QuerySet):
    def published(self):
        return self.filter(
            pub_date__lte=date.today())

#
# class PostManager(models.Manager):
#     def published(self):
#         return self.get_queryset().filter(
#             pub_date__lte=date.today())

#ConnectedPostManager=PostManager.from_queryset(PostQueryset)

PostManager=BasePostManager.from_queryset(PostQueryset)

class Post(models.Model):
    title=models.CharField(max_length=63)
    slug=models.SlugField(max_length=63,
                          help_text='A label for URL config',
                          unique_for_month='pub_date')
    text=models.TextField()
    pub_date=models.DateField('date_published',
                              auto_now_add=True)
    author=models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='blog_posts')

    objects= PostManager()

    def __str__(self):
        return "{} on {}".format(self.title,
                                 self.pub_date.strftime('%Y-%m-%d'))
    class Meta:
        verbose_name='blog_post'
        ordering=['-pub_date','title']
        get_latest_by='pub_date'
        index_together=('slug','pub_date')

    def get_absolute_url(self):
        return reverse('blog_post_detail',kwargs={
            'year':self.pub_date.year,
            'month':self.pub_date.month,
            'slug':self.slug})

    def get_update_url(self):
        return reverse('blog_post_update',
                       kwargs={
                           'year':self.pub_date.year,
                           'month':self.pub_date.month,
                           'slug':self.slug})
    def get_delete_url(self):
        return reverse('blog_post_delete',
                       kwargs={
                           'year':self.pub_date.year,
                           'month':self.pub_date.month,
                           'slug':self.slug})
    def natural_key(self):
        return (self.pub_date,self.slug)

    natural_key.dependencies=[
        'bloguser.user',
    ]

    def formatted_title(self):
        return self.title.title()

    def short_text(self):
        if len(self.text) > 20:
            short = ' '.join(self.text.split()[:20])
            short+=' ...'
        else:
            short=self.text
        return short

class Comment(models.Model):
    text=models.TextField()
    date_added=models.DateField('date_added',auto_now_add=True)
    #uuid=models.UUIDField(default=uuid.uuid4, editable=False)
    uuid=models.CharField('uuid',max_length=50)
    depth=models.IntegerField('depth',default=0)
    parent=models.ForeignKey("Comment",null=True,blank=True,default=None)

    # slug=models.SlugField(max_length=31,unique=True,
    #                       help_text='A label for URL config')
    post=models.ForeignKey(Post,on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='blog_comments')

    def __str__(self):
        return "Comment added on {}".format(
            self.date_added.strftime('%Y-%m-%d'))

    @property
    def depth_multiply(self):
        return self.depth*50

    class Meta:
        verbose_name='comment'
        ordering=['-date_added']
        get_latest_by='date_added'
        permissions=(
            ("view_child_comments",
             "Can view child comments"),
        )

