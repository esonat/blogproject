from django.contrib import admin

from .models import Post,Comment
from django.db.models import Count


# admin.site.register(Post)

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    date_hierarchy = 'pub_date'
    list_display = ('title','pub_date','comment_count')
    list_filter = ('pub_date',)
    search_fields = ('title','text')
    prepopulated_fields = {"slug":("title",)}
    #
    # def get_queryset(self, request):
    #     queryset=super().get_queryset(request)
    #     return queryset.annotate(
    #         count_number=Count('comment_set'))

    def comment_count(self,post):
        return post.comment_set.count()
    comment_count.short_description = 'Number of comments'

    fieldsets = (
        (None,{
            'fields':(
                'title','slug','author','text',
            )}),
    )

admin.site.register(Comment)