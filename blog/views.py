from django.shortcuts import render
from django.shortcuts import (get_object_or_404,render)
from .models import Post,Comment
from django.views.generic import CreateView,ListView,\
    YearArchiveView,View,\
    MonthArchiveView,ArchiveIndexView,\
    DateDetailView,DetailView,DeleteView
from django.views.decorators.http import require_http_methods
from .forms import PostForm,CommentForm
from django.shortcuts import get_object_or_404,render,redirect
from django.core.urlresolvers import reverse,reverse_lazy
from core.utils import UpdateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import PermissionDenied
from django.contrib.auth.decorators import user_passes_test
import logging
import uuid
from bloguser.decorators import (
    custom_login_required,
    require_authenticated_permission,
    class_login_required)
from .utils import PostFormValidMixin


logger=logging.getLogger(__name__)

def in_contrib_group(user):
    if user.groups.filter(name='contributors').exists():
        return True
    else:
        raise PermissionDenied

@require_authenticated_permission('blog.add_comment')
class CommentCreate(View):

    def post(self,request,*args,**kwargs):
        comment=Comment()
        comment.text=request.POST['text']
        year=kwargs.get('year')
        month=kwargs.get('month')
        slug=kwargs.get('slug')

        post=Post.objects.filter(pub_date__year=year,pub_date__month=month,slug__iexact=slug)[0]
        comment.post=post
        comment.uuid=uuid.uuid4()
        #comment.parent=None

        comment.save()

        return redirect(
            reverse('blog_post_detail',
                        kwargs={
                            'year':year,
                            'month':month,
                            'slug':slug,
                    }))

@require_authenticated_permission('blog.add_comment')
class ChildCommentCreate(View):
    def post(self,request,*args,**kwargs):
        parent_id=kwargs.get('parent_id')
        text=request.POST['text']

        year=request.POST['year']
        month=request.POST['month']
        slug=request.POST['slug']

        logger.error('year: %s, month:%s, slug=%s' % (year,month,slug))

        parent=Comment.objects.filter(uuid__iexact=parent_id)[0]
        parent_depth=parent.depth

        post=Post.objects.filter(pub_date__year=year,pub_date__month=month,slug__iexact=slug)[0]

        comment=Comment()
        comment.text=text
        comment.depth=parent_depth+1
        comment.uuid=uuid.uuid4()

        comment.parent=parent
        comment.post=post

        comment.save()

        return redirect(
            reverse('blog_post_detail',
                    kwargs={
                        'year':year,
                        'month':month,
                        'slug':slug,
                    }))

@require_authenticated_permission('blog.delete_comment')
class CommentDelete(View):
    def post(self,request,*args,**kwargs):

        year = request.POST['year']
        month = request.POST['month']
        slug = request.POST['slug']

        comment_id=kwargs['comment_id']
        #uuid_str='UUID(\''+uuid.UUID(comment_id).__str__()+'\')'
        logger.error(comment_id)

        comment=Comment.objects.filter(uuid__iexact=comment_id)[0]

        if comment:
            comment.delete()

        return redirect(
            reverse('blog_post_detail',
                       kwargs={
                           'year':year,
                           'month':month,
                           'slug':slug,
                       }))

@require_authenticated_permission('blog.add_post')
class PostCreate(PostFormValidMixin,CreateView):
    form_class = PostForm
    model=Post

@require_authenticated_permission('blog.delete_post')
class PostDelete(DeleteView):
    date_field='pub_date'
    model=Post
    success_url = reverse_lazy('blog_post_list')

class PostDetail(DetailView):
    form_class=CommentForm
    date_field='pub_date'
    model=Post
    queryset = (
        Post.objects
            .select_related('author__profile')
    )


class PostList(ListView):
    template_name='blog/post_list.html'
    model=Post
    paginate_by = 20

    def get(self,request):
        return render(
            request,
            self.template_name,
            {'post_list':Post.objects.all()})


@require_authenticated_permission('blog.change_post')
class PostUpdate(PostFormValidMixin,UpdateView):
    date_field='pub_date'
    form_class=PostForm
    model=Post