from django.db.models.signals import post_delete
from django.dispatch import receiver
from .models import Post, Comment
from django.core.mail import send_mail

@receiver(post_delete,sender=Post)
def send_mail_after_delete(sender,**kwargs):
    # send_mail('Post Deleted','Post deleted',
    #           'engin5@yahoo.com',['enginsonat86@hotmail.com'],
    #           fail_silently=False)
    pass