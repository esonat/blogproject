from django.conf.urls import url
from .views import PostCreate,PostDetail,\
    PostUpdate,PostDelete,PostList,CommentCreate,ChildCommentCreate,CommentDelete

urlpatterns=[

    url(r'^comment/(?P<comment_id>[^/]+)/'
        r'delete/$',
        CommentDelete.as_view(),
        name='blog_comment_delete'),
    url(r'^comment/(?P<parent_id>[^/]+)/'
        r'create/$',
        ChildCommentCreate.as_view(),
        name='blog_child_comment_create'),
    url(r'^(?P<year>\d{4})/'
        r'(?P<month>\d{1,2})/'
        r'(?P<slug>[\w\-]+)/'
        r'comment/create/$',
        CommentCreate.as_view(),
        name='blog_comment_create'),
    url(r'^$',
        PostList.as_view(),
        name='blog_post_list'),
    url(r'^create/$',
        PostCreate.as_view(),
        name='blog_post_create'),
    url(r'^(?P<year>\d{4})/'
        r'(?P<month>\d{1,2})/'
        r'(?P<slug>[\w\-]+)/$',
        PostDetail.as_view(),
        name='blog_post_detail'),
    url(r'^(?P<year>\d{4})/'
        r'(?P<month>\d{1,2})/'
        r'(?P<slug>[\w\-]+)/'
        r'update/$',
        PostUpdate.as_view(),
        name='blog_post_update'),
    url(r'^(?P<year>\d{4})/'
        r'(?P<month>\d{1,2})/'
        r'(?P<slug>[\w\-]+)/'
        r'delete/$',
        PostDelete.as_view(),
        name='blog_post_delete'),
]